#include <strings.h>

#include <Poco/URI.h>
#include <Poco/Net/Context.h>
#include "Poco/Net/HTTPSClientSession.h"

#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTTPMessage.h"
#include "Poco/Net/WebSocket.h"

#include <spdlog/spdlog.h>

#include <chrono>
#include <memory>
#include <iostream>
#include <vector>

bool isFrameFinished(int flags) {
	return static_cast<bool>(flags & Poco::Net::WebSocket::FRAME_FLAG_FIN);
}

ssize_t recv(Poco::Net::WebSocket& websocket, char* receiveBuffer, size_t receiveBufferLen) {
	ssize_t pos = 0;

	int flags;
//	do {
		flags = 0;
		ssize_t nbytes = websocket.receiveFrame(receiveBuffer + pos, receiveBufferLen - pos - 1, flags);
		pos += nbytes;
		receiveBuffer[pos] = 0;

		spdlog::info("recv: '{}'", receiveBuffer);
		spdlog::info("Frame received (length={} received={} flags=0x{}).", pos, nbytes, static_cast<unsigned>(flags));
//	} while (!isFrameFinished(flags) && pos > 0);

	return pos;
}

std::string buildRequest(const std::vector<std::string>& productIds) {
	const std::string prefix = R"({"type": "subscribe", "product_ids": [)";
	const std::string suffix = R"(], "channels": ["full"]})";

	std::string str;
	bool first{true};
	for (const std::string& productId : productIds) {
		if (first)
			first = false;
		else
			str += ",";

		str += '"' + productId + '"';
	}

	auto out = prefix + str + suffix;
	return out;
}

std::vector<std::string> instrumentIds(int argc, char* argv[]) {
	std::vector<std::string> out;

	for (int i = 1; i < argc; ++i)
		out.emplace_back(argv[i]);

	return out;
}

int main(int argc, char **argv)
try {
	// https://spdlog.docsforge.com/v1.x/3.custom-formatting/#pattern-flags
	spdlog::set_pattern("%Y-%m-%d %T:%S.%f %^%L%$ %v");

	const std::string url{ "ws://localhost:9980/market-data" };
	Poco::URI uri{ url };

//	auto* context = new Poco::Net::Context(Poco::Net::Context::CLIENT_USE, "", "", "", Poco::Net::Context::VERIFY_NONE, 0, true);
	Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());;

	Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_GET, uri.getPathAndQuery(), Poco::Net::HTTPMessage::HTTP_1_1);

	Poco::Net::HTTPResponse response;
	Poco::Net::WebSocket websocket(session, request, response);
//	websocket.setReceiveTimeout(Poco::Timespan(std::chrono::seconds(0).count(), std::chrono::microseconds(500000).count()));
	websocket.setReceiveTimeout(Poco::Timespan(std::chrono::seconds(60).count(), std::chrono::microseconds(500000).count()));

	const auto wsRequest = buildRequest(instrumentIds(argc, argv));
	spdlog::info("send: '{}'", wsRequest);
	ssize_t nbytes = websocket.sendFrame(wsRequest.c_str(), static_cast<int>(wsRequest.size()));
	spdlog::info("Frame sent (length={}).", nbytes);

	constexpr size_t receiveBufferSize{ 256 * 1024 };
	std::unique_ptr<char[]> receiveBuffer{ std::make_unique<char[]>(receiveBufferSize) };
	do {
		bzero(receiveBuffer.get(), receiveBufferSize);
		int flags{};

		nbytes = websocket.receiveFrame(receiveBuffer.get(), receiveBufferSize, flags);
//		nbytes = recv(websocket, receiveBuffer.get(), receiveBufferSize);
		if (nbytes > 0) {
			receiveBuffer.get()[nbytes] = 0;
		}

		spdlog::info("recv: {}", receiveBuffer.get());
//		spdlog::info("recv: '{}'", receiveBuffer);
		spdlog::info("Frame received (received={} flags=0x{}).", nbytes, static_cast<unsigned>(flags));
	} while (nbytes > 0);
}
catch (const std::exception& e) {
	spdlog::error("fatal: {}", e.what());
}
