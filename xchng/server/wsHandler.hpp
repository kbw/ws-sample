#pragma once

#include "Orderbook.hpp"

#include <Poco/Net/HTTPRequestHandler.h>

namespace Poco::Net {
	class HTTPServerRequest;
	class HTTPServerResponse;
	class WebSocket;
}

struct Command;

/// Handle a WebSocket connection.
class WebSocketRequestHandler : public Poco::Net::HTTPRequestHandler
{
public:
	void handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response);

	void doSubscribe(Poco::Net::WebSocket& ws, const Command& cmd);
	void doUnsubscribe(Poco::Net::WebSocket& ws, const Command& cmd);
	void doError(Poco::Net::WebSocket& ws, const Command& cmd);

	static ssize_t send(Poco::Net::WebSocket& ws, const char* buffer, ssize_t size);

	static inline OrderbookManager orderbookMgr_;
};
