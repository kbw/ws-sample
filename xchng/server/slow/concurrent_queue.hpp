#pragma once

#include <list>
#include <mutex>

namespace slow {

template <typename T>
struct concurrent_queue {
	std::size_t size() const {
		std::lock_guard lock{inuse_};
		return cont_.size();
	}

	void push(T val) {
		std::lock_guard lock{inuse_};
		cont_.emplace_back(std::move(val));
	}

	bool pop(T& val) {
		std::lock_guard lock{inuse_};
		if (cont_.empty())
			return false;
		std::swap(cont_.front(), val);
		cont_.pop_front();
		return true;
	}

	mutable std::mutex inuse_;
	std::list<T> cont_;
};

}
