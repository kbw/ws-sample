#pragma once

#include "slow/concurrent_queue.hpp"

#include <chrono>
#include <map>
#include <thread>

#include <assert.h>

enum class Side { bid, ask };

struct Order {
	Order(Side side, double price, double size) :
		side(side), price(price), size(size) {
	}
	Order() = delete;
	Order(const Order& ) = default;

	Side side;
	double price;
	double size;
};

struct Orderbook {
    using Timestamp = std::chrono::time_point<std::chrono::steady_clock>;
    using OrderbookSide = std::map<Timestamp, Order>;

	void add(const Order& order) {
		auto now = std::chrono::steady_clock::now();
		switch (order.side) {
		case Side::bid:
			bids_.emplace(std::piecewise_construct, std::forward_as_tuple(now), std::forward_as_tuple(order));
			break;
		case Side::ask:
			bids_.emplace(std::piecewise_construct, std::forward_as_tuple(now), std::forward_as_tuple(order));
			break;
    	}
    }

	OrderbookSide bids_;
	OrderbookSide asks_;
};

struct OrderbookManager {
	OrderbookManager() : receiver_(std::thread{
			}) {
	}

	~OrderbookManager() {
		receiver_.join();
	}

	Orderbook& operator[](const std::string& key) {
		return orderbooks_[key];
	}

	std::map<std::string, Orderbook> orderbooks_; // symbol -> orderbook
	slow::concurrent_queue<Order> queue_;
	std::thread receiver_;
};

inline std::string to_string(Side side) {
	return side == Side::bid ? "bid" : "ask";
}
inline std::string to_json(Side side) {
	return R"("side": )" + to_string(side);
}

inline std::string to_string(const Order& order) {
	return	"{" +
			to_string(order.side) + ":" + std::to_string(order.price)  + ":" + std::to_string(order.size) +
			"}";
}
inline std::string to_json(const Order& order) {
	return	"{" +
			to_json(order.side) +
			R"(, "price": ")" + std::to_string(order.price) +
			R"(, "size": )" + std::to_string(order.size) +
			"}";
}

template <class OrderbookSide>
inline std::string to_string(const OrderbookSide& collection) {
	std::string out;

    for (const auto& order : collection) {
		if (out.size())
			out += ",";
		out += to_string(order.second);
	}

    return out;
}
template <typename OrderbookSide>
inline std::string to_json(const OrderbookSide& collection) {
	std::string out = "[";

	for (const auto& order : collection) {
		if (out.size() > 1)
			out += ",";
		out += to_json(order.second);
	}

	return out + "]";
}

inline std::string to_string(const Orderbook& orderbook) {
	std::string bids = "bids: " + to_string(orderbook.bids_);
	std::string asks = "asks: " + to_string(orderbook.asks_);
	return bids + "\n" + asks + "\n";
}
inline std::string to_json(const Orderbook& orderbook) {
	std::string bids = R"({"bids": )" + to_json(orderbook.bids_) + "}";
	std::string asks = R"({"asks": )" + to_json(orderbook.asks_) + "}";
	return bids + "," + asks;
}

inline std::string to_string(const OrderbookManager& objs) {
	std::string out;

	for (const auto& obj : objs.orderbooks_) {
		if (out.size())
			out += ",";
		out += obj.first + ": " + to_json(obj.second) + "\n";
	}

	return out;
}
inline std::string to_json(const OrderbookManager& objs) {
	std::string out = "{";

	for (const auto& obj : objs.orderbooks_) {
		if (out.size() > 1)
			out += ",";
		out += "\"" + obj.first + "\": " + to_json(obj.second);
	}

	return out + "}";
}
/*
 */
