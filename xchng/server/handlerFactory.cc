//
// WebSocketServer.cpp
//
// This sample demonstrates the WebSocket class.
//
// Copyright (c) 2012, Applied Informatics Software Engineering GmbH.
// and Contributors.
//
// SPDX-License-Identifier:	BSL-1.0
//
#include "handlerFactory.hpp"
#include "httpHandler.hpp"
#include "wsHandler.hpp"

#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPRequestHandler.h>

/*
#include "orderbook.hpp"

#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPServerParams.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPServerParams.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/Net/WebSocket.h>
#include <Poco/Net/NetException.h>
#include <Poco/Util/ServerApplication.h>
#include <Poco/Util/Option.h>
#include <Poco/Util/OptionSet.h>
#include <Poco/Util/HelpFormatter.h>

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>

#include <fmt/format.h>

#include <memory>
#include <iostream>
#include <vector>

#include <stdint.h>
 */

#include <spdlog/spdlog.h>

Poco::Net::HTTPRequestHandler* RequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest& request) {
	// print request parameters
 	spdlog::info("Request from {}: {} {} {}",
		request.clientAddress().toString(),
		request.getMethod(),
		request.getURI(),
		request.getVersion());

	// print connection parameters
	for (Poco::Net::HTTPServerRequest::ConstIterator it = request.begin(); it != request.end(); ++it)
		spdlog::info("{}: {}", it->first, it->second);

	// handle request: websocket
	if (request.find("Upgrade") != request.end() && Poco::icompare(request["Upgrade"], "websocket") == 0) {
		spdlog::info("Creating ws handler");
		return new WebSocketRequestHandler;
	}

	// handle request: http
	spdlog::info("Creating http handler");
	return new PageRequestHandler;
}
