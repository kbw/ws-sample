#pragma once

#include <Poco/Net/HTTPRequestHandler.h>

namespace Poco::Net {
	class HTTPServerRequest;
	class HTTPServerResponse;
}

/// Return a HTML document with some JavaScript creating a WebSocket connection.
class PageRequestHandler : public Poco::Net::HTTPRequestHandler
{
public:
	void handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response);
};
