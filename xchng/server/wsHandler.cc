//
// WebSocketServer.cpp
//
// This sample demonstrates the WebSocket class.
//
// Copyright (c) 2012, Applied Informatics Software Engineering GmbH.
// and Contributors.
//
// SPDX-License-Identifier:	BSL-1.0
//
#include "wsHandler.hpp"
//#include "Engine.hpp"
//#include "Orderbook.hpp"

#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/WebSocket.h>
#include <Poco/Net/NetException.h>
#include <Poco/Util/ServerApplication.h>
/*
#include "orderbook.hpp"

#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPServerParams.h>
#include <Poco/Net/HTTPServerParams.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/Util/Option.h>
#include <Poco/Util/OptionSet.h>
#include <Poco/Util/HelpFormatter.h>
 */

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>

#include <fmt/format.h>

#include <spdlog/spdlog.h>

#include <memory>
#include <iostream>
#include <string>
#include <vector>

struct Command {
	enum class Operation { Unknown, Subscribe, Unsubscribe };
	using Symbols = std::vector<std::string>;

	Operation opcode{ Operation::Unknown };
	std::string error{ "no error" };
	Symbols symbols;
	Symbols details;
};

/// Handle a WebSocket connection.
Command createFromJson(char* buffer, size_t /*size*/) {
	Command cmd;

	rapidjson::Document jdoc;
	jdoc.ParseInsitu(buffer);

	#define productIds "product_ids"
	#define type "type"
	#define subscribe "subscribe"
	#define unsubscribe "unsubscribe"
	#define channels "channels"

	if (jdoc.HasMember(productIds) && jdoc[productIds].IsArray()) {
		const auto& array = jdoc[productIds];
		for (rapidjson::SizeType i = 0; i != array.Size(); ++i)
			cmd.symbols.emplace_back(array[i].GetString());
	}

	if (jdoc.HasMember(channels) && jdoc[channels].IsArray()) {
		const auto& array = jdoc[channels];
		for (rapidjson::SizeType i = 0; i != array.Size(); ++i)
			cmd.details.emplace_back(array[i].GetString());
	}

	if (jdoc.HasMember(type) && jdoc[type].IsString()) {
		std::string typeStr = jdoc[type].GetString();
		if (typeStr == subscribe)
			cmd.opcode = Command::Operation::Subscribe;
		else if (typeStr == unsubscribe)
			cmd.opcode = Command::Operation::Unsubscribe;
		else
			cmd.error = "bad command: " + typeStr;
	}

	#undef productIds
	#undef type
	#undef subscribe
	#undef unsubscribe
	#undef channels

	return cmd;
}

void WebSocketRequestHandler::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response) {
	try {
		Poco::Net::WebSocket ws(request, response);
		spdlog::info("WebSocket connection established.");

		int flags;
		ssize_t nbytes;
		do {
			// do recv
			flags = 0;
			constexpr size_t buffer_size = 128;
			char buffer[buffer_size];
			nbytes = ws.receiveFrame(buffer, buffer_size - 1, flags);
			buffer[nbytes] = 0;
			spdlog::info("Frame received (length={}, flags={}).", nbytes, static_cast<unsigned>(flags));
			spdlog::info("recv: '{}'", buffer);

			Command cmd = createFromJson(buffer, nbytes);
			switch (cmd.opcode) {
			case Command::Operation::Subscribe:
				doSubscribe(ws, cmd);
				break;
			case Command::Operation::Unsubscribe:
				doUnsubscribe(ws, cmd);
				break;
			case Command::Operation::Unknown:
			default:
				doError(ws, cmd);
				break;
			}

/*
			// prep reply
			if (nbytes > 0) {
//				nbytes += snprintf(&buffer[nbytes - 1], sizeof(buffer) - nbytes, ", \"flags\": 0x%x}", flags);
			}

			while (cmd.opcode != Command::Operation::Unknown) {
				spdlog::info("send: '{}'", buffer.get());
//				flags = Poco::Net::WebSocket::FRAME_FLAG_FIN;
				flags = 0;
				ssize_t sentBytes = ws.sendFrame(buffer.get(), nbytes, flags);
				spdlog::info("Frame send (length={}, flags=0x{}).", sentBytes, static_cast<unsigned>(flags));
				using namespace std::chrono;
				std::this_thread::sleep_for(1s);
			}
 */
/*
{"type": "subscribe", "product_ids": ["BTC-USD","BTC-EUR"], "channels": ["full"]}
{"type": "subscribe", "product_ids": ["BTC-USD","BTC-EUR"], "channels": ["full"], "flags": 0x81}
			rapidjson::Document jdoc;2ddu
			jdoc.ParseInsitu(buffer);
			if (jdoc.HasMember("type") && jdoc["type"] == "subscribe") {
				auto reply{ fmt::format("{\"reply\": \"received request\"}") };
				flags = oldflags;
				strcpy(bufferOut.get(), reply.c_str());
				ws.sendFrame(bufferOut.get(), n, flags);
			}
 */
		} while (nbytes > 0 &&
				 (flags & Poco::Net::WebSocket::FRAME_OP_BITMASK) != Poco::Net::WebSocket::FRAME_OP_CLOSE);
		spdlog::info("WebSocket connection closed.");
	} catch (Poco::Net::WebSocketException& exc) {
		spdlog::error("websocket error: {}", exc.what());
		switch (exc.code()) {
		case Poco::Net::WebSocket::WS_ERR_HANDSHAKE_UNSUPPORTED_VERSION:
			response.set("Sec-WebSocket-Version", Poco::Net::WebSocket::WEBSOCKET_VERSION);
			// fallthrough
		case Poco::Net::WebSocket::WS_ERR_NO_HANDSHAKE:
		case Poco::Net::WebSocket::WS_ERR_HANDSHAKE_NO_VERSION:
		case Poco::Net::WebSocket::WS_ERR_HANDSHAKE_NO_KEY:
			response.setStatusAndReason(Poco::Net::HTTPResponse::HTTP_BAD_REQUEST);
			response.setContentLength(0);
			response.send();
			break;
		}
	}
}

void WebSocketRequestHandler::doSubscribe(Poco::Net::WebSocket& ws, const Command& cmd) {
	std::string str;
	for (const std::string& symbol : cmd.symbols) {
		const auto& orderbook = orderbookMgr_[symbol];
        spdlog::info("orderbook: {} : {}", symbol, to_string(orderbook));

		if (str.size())
			str += ", ";
		str += symbol;
	}

	constexpr size_t buffer_size = 128;
	char buffer[buffer_size];
	ssize_t nbytes = snprintf(buffer, buffer_size, "{\"subscribe\": \"%s\"}", str.c_str());
	spdlog::info("send: '{}'", buffer);
	send(ws, buffer, nbytes);
}

void WebSocketRequestHandler::doUnsubscribe(Poco::Net::WebSocket& ws, const Command& cmd) {
	std::string str;
	bool first = true;
	for (const std::string& symbol : cmd.symbols) {
		if (first)
			first = false;
		else
			str += ", ";
		str += symbol;
	}

	constexpr size_t buffer_size = 128;
	char buffer[buffer_size];
	ssize_t nbytes = snprintf(buffer, buffer_size, "{\"unsubscribe\": \"%s\"}", str.c_str());
	spdlog::info("send: '{}'", buffer);
	send(ws, buffer, nbytes);
}

void WebSocketRequestHandler::doError(Poco::Net::WebSocket& ws, const Command& cmd) {
	constexpr size_t buffer_size = 128;
	char buffer[buffer_size];
	ssize_t nbytes = snprintf(buffer, buffer_size, "{\"error\": \"%s\"}", cmd.error.c_str());
	spdlog::info("send: '{}'", buffer);
	send(ws, buffer, nbytes);
}

ssize_t WebSocketRequestHandler::send(Poco::Net::WebSocket& ws, const char* buffer, ssize_t size) {
	int flags = 0;
//	int flags = Poco::Net::WebSocket::FRAME_FLAG_FIN;
	ssize_t sentBytes = ws.sendFrame(buffer, size, flags);
	spdlog::info("Frame send (length={}, flags=0x{}).", sentBytes, static_cast<unsigned>(flags));

//	using namespace std::chrono;
//	std::this_thread::sleep_for(1s);
	return sentBytes;
}
