#include <strings.h>
#include <sys/time.h>

#include <Poco/URI.h>
#include <Poco/Net/Context.h>
#include "Poco/Net/HTTPSClientSession.h"

#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTTPMessage.h>
#include <Poco/Net/WebSocket.h>

#include <spdlog/spdlog.h>

#include <chrono>
#include <memory>
#include <iostream>
#include <vector>

#include <stdio.h>

bool isFrameFinished(int flags) {
	return static_cast<bool>(flags & Poco::Net::WebSocket::FRAME_FLAG_FIN);
}

ssize_t recv(Poco::Net::WebSocket& websocket, std::string_view buffer) {
	ssize_t pos = 0;
	bzero(const_cast<char*>(buffer.data()), buffer.size());

	int flags;
//	do {
		flags = 0;
		ssize_t nbytes = websocket.receiveFrame(const_cast<char*>(buffer.data()) + pos, buffer.size() - pos - 1, flags);
		if (nbytes != -1) {
			pos += nbytes;
			const_cast<char*>(buffer.data())[pos] = 0;
		}

		SPDLOG_INFO("Frame received (flags=0x{} nbytes={} data={}).", static_cast<unsigned>(flags), nbytes, buffer.data());
//	} while (!isFrameFinished(flags) && pos > 0);

	return pos;
}

std::vector<std::string> instrumentIds(int argc, char* argv[]) {
	std::vector<std::string> out;

	for (int i = 1; i < argc; ++i)
		out.emplace_back(argv[i]);

	return out;
}

std::string buildRequest(const char* verb, std::size_t seed = 2) {
	constexpr auto requestTemplate = R"({"type": "%s", "seed": %d})";

	constexpr std::size_t bufsz = 64;
	char buf[bufsz];
	int len = snprintf(buf, bufsz, requestTemplate, verb, static_cast<int>(seed));

	return {buf, static_cast<std::size_t>(len)};
}

int main(int argc, char **argv)
try {
	// https://spdlog.docsforge.com/v1.x/3.custom-formatting/#pattern-flags
	spdlog::set_pattern("%Y-%m-%d %T:%S.%f %^%L%$ %v");

	const std::string url{ "ws://localhost:9981/random" };
	Poco::URI uri{ url };

//	auto* context = new Poco::Net::Context(Poco::Net::Context::CLIENT_USE, "", "", "", Poco::Net::Context::VERIFY_NONE, 0, true);
//	Poco::Net::HTTPSClientSession session(uri.getHost(), uri.getPort(), context);
	Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());

	// uri.getPathAndQuery() - /random
	Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_GET, uri.getPathAndQuery(), Poco::Net::HTTPMessage::HTTP_1_1);

	Poco::Net::HTTPResponse response;
	Poco::Net::WebSocket websocket(session, request, response);
	websocket.setReceiveTimeout(Poco::Timespan(std::chrono::seconds(20).count(), std::chrono::microseconds(500000).count()));

	constexpr size_t receiveBufferSize{ 256 * 1024 };
	std::unique_ptr<char[]> receiveBuffer{ std::make_unique<char[]>(receiveBufferSize) };
	ssize_t nbytes;

	{
		const auto wsRequest = buildRequest("subscribe", time(nullptr));
		SPDLOG_INFO("send: '{}'", wsRequest);
		ssize_t nbytes = websocket.sendFrame(wsRequest.c_str(), static_cast<int>(wsRequest.size()), Poco::Net::WebSocket::FRAME_OP_TEXT);
		SPDLOG_INFO("Frame sending={} sent={}", wsRequest.size(), nbytes);
	}
//	for (std::size_t i = 0; i != 5; ++i) {
	while (true) {
		SPDLOG_INFO("Prep for receive");
		bzero(receiveBuffer.get(), receiveBufferSize);
		int flags{};

		nbytes = websocket.receiveFrame(receiveBuffer.get(), receiveBufferSize, flags);
		if (nbytes != -1) {
			receiveBuffer.get()[nbytes] = 0;
		}
		if (nbytes == 0) {
			SPDLOG_INFO("Frame received (flags=0x{} nbytes={} sleeping).", static_cast<unsigned>(flags), nbytes);
			using namespace std::chrono_literals;
			std::this_thread::sleep_for(1s);
		}
		SPDLOG_INFO("Frame received (flags=0x{} nbytes={} data={}).", static_cast<unsigned>(flags), nbytes, receiveBuffer.get());
	}

/*
	{
		const auto wsRequest = buildRequest("unsubscribe");
		SPDLOG_INFO("send: '{}'", wsRequest);
		ssize_t nbytes = websocket.sendFrame(wsRequest.c_str(), static_cast<int>(wsRequest.size()));
		SPDLOG_INFO("Frame sending={} sent={}", wsRequest.size(), nbytes);
	}
	do {
		bzero(receiveBuffer.get(), receiveBufferSize);
		int flags{};

		nbytes = websocket.receiveFrame(receiveBuffer.get(), receiveBufferSize, flags);
		if (nbytes != -1) {
			receiveBuffer.get()[nbytes] = 0;
		}
		SPDLOG_INFO("Frame received (flags=0x{} nbytes={} data={}).", static_cast<unsigned>(flags), nbytes, receiveBuffer.get());
	} while (nbytes > 0);
 */
}
catch (const std::exception& e) {
	SPDLOG_ERROR("fatal: {}", e.what());
}
