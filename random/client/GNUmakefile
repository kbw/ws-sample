PROGS_CXX   = ws-client
DEPENDS     = ws-client.depends
REPORT      = ws-client.report
SRCS.ws-client = ws-client.cc

PREFIX     ?= /usr/local
D          ?=

CXXFLAGS    = -g
CXXFLAGS   += -std=c++17 -pedantic -pthread -I$(PREFIX)/include -DSPDLOG_FMT_EXTERNAL=1
CXXFLAGS   += -Wall -Wextra -Wno-unused-parameter
LDADD = -L$(PREFIX)/lib -lPocoNetSSL$(D) -lPocoNet$(D) -lPocoFoundation$(D) -lPocoUtil$(D)
LDADD += -lspdlog$(D) -lfmt$(D)

INFO       ?= clang-tidy
INFOFLAGS  ?= -checks='*,-llvmlibc-restrict-system-libc-headers,-llvmlibc-implementation-in-namespace,-llvmlibc-callee-namespace,-llvm-include-order,-modernize-use-trailing-return-type,-google-readability-braces-around-statements,-hicpp-braces-around-statements,-readability-braces-around-statements,-fuchsia-overloaded-operator,-fuchsia-default-arguments-calls,-google-explicit-constructor,-hicpp-explicit-conversions,-fuchsia-default-arguments-declarations,-misc-non-private-member-variables-in-classes'

.PHONY: $(DEPENDS) $(PROGS_CXX) $(REPORT)

all: $(DEPENDS) $(PROGS_CXX)

clean:
	- rm $(DEPENDS) $(PROGS_CXX) $(REPORT) \
	  $(SRCS.ws-client:.cc=.depend) \
	  $(SRCS.ws-client:.cc=.info) \
	  $(SRCS.ws-client:.cc=.o)

ws-client: $(SRCS.ws-client:.cc=.o)
	$(LINK.cc) -o $@ $^ $(LDADD)

ws-client.depends: $(SRCS.ws-client:.cc=.depend)

ws-client.report: $(SRCS.ws-client:.cc=.info)

%.info: %.cc
	$(INFO) $(INFOFLAGS) $< > $@

%.depends: %.depend
	cat $< > $@

%.depend: %.cc
	$(COMPILE.cc) -o $@ -MM $<

-include $(DEPENDS)
