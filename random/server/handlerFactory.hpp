#pragma once

#include <Poco/Net/HTTPRequestHandlerFactory.h>

namespace Poco::Net {
	class HTTPServerRequest;
	class HTTPRequestHandler;
}

class RequestHandlerFactory: public Poco::Net::HTTPRequestHandlerFactory
{
public:
	Poco::Net::HTTPRequestHandler* createRequestHandler(const Poco::Net::HTTPServerRequest& request);
};
