//
// WebSocketServer.cpp
//
// This sample demonstrates the WebSocket class.
//
// Copyright (c) 2012, Applied Informatics Software Engineering GmbH.
// and Contributors.
//
// SPDX-License-Identifier:	BSL-1.0
//
#include "wsHandler.hpp"

#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/WebSocket.h>
#include <Poco/Net/NetException.h>
#include <Poco/Util/ServerApplication.h>

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>

#include <fmt/format.h>
#include <spdlog/spdlog.h>

#include <chrono>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

struct Command {
	enum class Operation { Unknown, Subscribe, Unsubscribe };
	using Symbols = std::vector<std::string>;

	Operation opcode{ Operation::Unknown };
	std::string error{ "no error" };
	int seed{0};
	Symbols details;
};

/// Handle a WebSocket connection.
Command createFromJson(std::string_view json) {
	Command cmd;

	rapidjson::Document jdoc;
	SPDLOG_INFO("createFromJson: {}", json.data());
	if (jdoc.Parse(json.data()).HasParseError()) {
		SPDLOG_ERROR("createFromJson: parse error");
		throw std::runtime_error("parse error");
	}

	constexpr auto seed = "seed";
	constexpr auto type = "type";
	constexpr auto subscribe = "subscribe";
	constexpr auto unsubscribe = "unsubscribe";

	if (jdoc.HasMember(seed) && jdoc[seed].IsInt()) {
		SPDLOG_INFO("createFromJson: {} exists", seed);
		cmd.seed = jdoc[seed].GetInt();
		SPDLOG_INFO("createFromJson: {} : {}", seed, cmd.seed);
	}

	if (jdoc.HasMember(type) && jdoc[type].IsString()) {
		std::string typeStr = jdoc[type].GetString();
		if (typeStr == subscribe)
			cmd.opcode = Command::Operation::Subscribe;
		else if (typeStr == unsubscribe)
			cmd.opcode = Command::Operation::Unsubscribe;
		else
			cmd.error = "bad command: " + typeStr;
	}

	return cmd;
}

std::string threadid() {
	std::ostringstream os;
	os << std::this_thread::get_id();
	return os.str();
}

void WebSocketRequestHandler::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response) {
	try {
		Poco::Net::WebSocket ws(request, response);
		SPDLOG_INFO("WebSocket connection established.");

		int flags;
		ssize_t nbytes;
		do {
			// do recv
			flags = 0;
			constexpr size_t buffer_size = 128;
			char buffer[buffer_size];
			nbytes = ws.receiveFrame(buffer, buffer_size - 1, flags);
			buffer[nbytes] = 0;
			SPDLOG_INFO("Frame received (size={}, flags={}) recv={}", nbytes, static_cast<unsigned>(flags), buffer);

			if (nbytes == 0) {
				ClientData client_data;
				if (queue_.try_pop(client_data)) {
					auto reply = buildReply(client_data.seqno, client_data.seed);
					send(ws, reply);
				}
			} else {
				Command cmd = createFromJson({buffer, static_cast<std::size_t>(nbytes)});
				switch (cmd.opcode) {
				case Command::Operation::Subscribe:
					SPDLOG_INFO("subscribe: threadid={}", threadid());
					doSubscribe(ws, cmd);
					break;
				case Command::Operation::Unsubscribe:
					SPDLOG_INFO("subscribe: threadid={}", threadid());
					doUnsubscribe(ws, cmd);
					break;
				case Command::Operation::Unknown:
				default:
					doError(ws, cmd);
					break;
				}
			}
		} while (nbytes > 0 &&
				 (flags & Poco::Net::WebSocket::FRAME_OP_BITMASK) != Poco::Net::WebSocket::FRAME_OP_CLOSE);
		SPDLOG_INFO("WebSocket connection closed.");
	} catch (Poco::Net::WebSocketException& exc) {
		SPDLOG_ERROR("websocket error: {}", exc.what());
		switch (exc.code()) {
		case Poco::Net::WebSocket::WS_ERR_HANDSHAKE_UNSUPPORTED_VERSION:
			response.set("Sec-WebSocket-Version", Poco::Net::WebSocket::WEBSOCKET_VERSION);
			// fallthrough
		case Poco::Net::WebSocket::WS_ERR_NO_HANDSHAKE:
		case Poco::Net::WebSocket::WS_ERR_HANDSHAKE_NO_VERSION:
		case Poco::Net::WebSocket::WS_ERR_HANDSHAKE_NO_KEY:
			response.setStatusAndReason(Poco::Net::HTTPResponse::HTTP_BAD_REQUEST);
			response.setContentLength(0);
			response.send();
			break;
		}
	}
}

std::string WebSocketRequestHandler::buildReply(std::size_t seqno, int value) {
	constexpr auto requestTemplate = R"({"seqno": %d, "value": %d})";

	constexpr std::size_t bufsz = 64;
	char buf[bufsz];
	int len = snprintf(buf, bufsz, requestTemplate, static_cast<int>(seqno), value);

	return {buf, static_cast<std::size_t>(len)};
}

void WebSocketRequestHandler::run(Poco::Net::WebSocket& ws, int seed) {
	for (std::size_t seqno = 1; !stop_thread_; ++seqno) {
//		SPDLOG_INFO("run: {}:{}.", seqno, seed);
		queue_.push({seqno, seed});

		using namespace std::chrono_literals;
		std::this_thread::sleep_for(1s);
	}
}

void WebSocketRequestHandler::doSubscribe(Poco::Net::WebSocket& ws, const Command& cmd) {
	if (!thread_) {
		stop_thread_ = false;
		thread_ = std::make_unique<std::thread>([&]{
				run(ws, cmd.seed);
			});
		SPDLOG_INFO("doSubscribe: started thread");
		return;
	}

	throw std::runtime_error("WebSocketRequestHandler::run already running");
}

void WebSocketRequestHandler::doUnsubscribe(Poco::Net::WebSocket& ws, const Command& cmd) {
	if (thread_) {
		stop_thread_ = true;
		thread_->join();
		thread_.reset();
	}
}

void WebSocketRequestHandler::doError(Poco::Net::WebSocket& ws, const Command& cmd) {
	constexpr size_t buffer_size = 128;
	char buffer[buffer_size];
	int nbytes = snprintf(buffer, buffer_size, "{\"error\": \"%s\"}", cmd.error.c_str());
	SPDLOG_INFO("send: '{}'", buffer);
	send(ws, {buffer, static_cast<std::size_t>(nbytes)});
}

ssize_t WebSocketRequestHandler::send(Poco::Net::WebSocket& ws, std::string_view msg) {
	int flags = Poco::Net::WebSocket::FRAME_OP_TEXT;
	ssize_t sentBytes = ws.sendFrame(msg.data(), msg.size(), flags);
	SPDLOG_INFO("Frame send (length={}, flags=0x{}, msg={})", sentBytes, static_cast<unsigned>(flags), msg.data());
	return sentBytes;
}
