//
// WebSocketServer.cpp
//
// This sample demonstrates the WebSocket class.
//
// Copyright (c) 2012, Applied Informatics Software Engineering GmbH.
// and Contributors.
//
// SPDX-License-Identifier:	BSL-1.0
//
#include "handlerFactory.hpp"
#include "wsHandler.hpp"

#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPRequestHandler.h>

#include <spdlog/spdlog.h>

Poco::Net::HTTPRequestHandler* RequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest& request) {
	// print request parameters
 	SPDLOG_INFO("Request from {}: {} {} {}",
		request.clientAddress().toString(),
		request.getMethod(),
		request.getURI(),
		request.getVersion());

	// print connection parameters
	for (Poco::Net::HTTPServerRequest::ConstIterator it = request.begin(); it != request.end(); ++it)
		SPDLOG_INFO("{}: {}", it->first, it->second);

	// handle request: websocket
	if (request.find("Upgrade") != request.end() && Poco::icompare(request["Upgrade"], "websocket") == 0) {
		SPDLOG_INFO("Creating ws handler");
		return new WebSocketRequestHandler;
	}

	// handle request: http
//	SPDLOG_INFO("Creating http handler");
//	return new PageRequestHandler;
	return nullptr;
}
