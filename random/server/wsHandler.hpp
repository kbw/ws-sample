#pragma once

#include "slow/concurrent_queue.hpp"

#include <Poco/Net/HTTPRequestHandler.h>

#include <atomic>
#include <memory>
#include <thread>

namespace Poco::Net {
	class HTTPServerRequest;
	class HTTPServerResponse;
	class WebSocket;
}

struct Command;

struct ClientData {
	ClientData() = default;
	ClientData(std::size_t seqno, int seed) : seqno(seqno), seed(seed) {
	}

	std::size_t seqno;
	int seed;
};

/// Handle a WebSocket connection.
class WebSocketRequestHandler : public Poco::Net::HTTPRequestHandler
{
public:
	void handleRequest(Poco::Net::HTTPServerRequest& req, Poco::Net::HTTPServerResponse& resp);

	void doSubscribe(Poco::Net::WebSocket& ws, const Command& cmd);
	void doUnsubscribe(Poco::Net::WebSocket& ws, const Command& cmd);
	void doError(Poco::Net::WebSocket& ws, const Command& cmd);

protected:
	virtual void run(Poco::Net::WebSocket& ws, int seed);

	static std::string buildReply(std::size_t seqno, int value);
	static ssize_t send(Poco::Net::WebSocket& ws, std::string_view msg);

private:
	std::atomic<bool> stop_thread_{};
	std::unique_ptr<std::thread> thread_{};
	slow::concurrent_queue<ClientData> queue_;
};
