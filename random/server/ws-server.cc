//
// WebSocketServer.cpp
//
// This sample demonstrates the WebSocket class.
//
// Copyright (c) 2012, Applied Informatics Software Engineering GmbH.
// and Contributors.
//
// SPDX-License-Identifier:	BSL-1.0
//
#include "handlerFactory.hpp"

#include <Poco/Util/ServerApplication.h>
#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/Util/Option.h>
#include <Poco/Util/OptionSet.h>
#include <Poco/Util/HelpFormatter.h>

#include <spdlog/spdlog.h>

#include <cstdint>
#include <iostream>

#include <stdint.h>

class WebSocketServer : public Poco::Util::ServerApplication
	/// The main application class.
	///
	/// This class handles command-line arguments and
	/// configuration files.
	/// Start the WebSocketServer executable with the help
	/// option (/help on Windows, --help on Unix) for
	/// the available command line options.
	///
	/// To use the sample configuration file (WebSocketServer.properties),
	/// copy the file to the directory where the WebSocketServer executable
	/// resides. If you start the debug version of the WebSocketServer
	/// (WebSocketServerd[.exe]), you must also create a copy of the configuration
	/// file named WebSocketServerd.properties. In the configuration file, you
	/// can specify the port on which the server is listening (default
	/// 9980) and the format of the date/time string sent back to the client.
	///
	/// To test the WebSocketServer you can use any web browser (http://localhost:9980/).
{
public:
	WebSocketServer() {
		// https://spdlog.docsforge.com/v1.x/3.custom-formatting/#pattern-flags
		spdlog::set_pattern("%Y-%m-%d %T:%S.%f %^%L%$ %@ %t %v");
	}

protected:
	void initialize(Poco::Util::Application& self) {
		loadConfiguration(); // load default configuration files, if present
		Poco::Util::ServerApplication::initialize(self);
	}

	void uninitialize() {
		Poco::Util::ServerApplication::uninitialize();
	}

	void defineOptions(Poco::Util::OptionSet& options) {
		Poco::Util::ServerApplication::defineOptions(options);

		options.addOption(
			Poco::Util::Option("help", "h", "display help information on command line arguments")
				.required(false)
				.repeatable(false));
	}

	void handleOption(const std::string& name, const std::string& value) {
		Poco::Util::ServerApplication::handleOption(name, value);

		if (name == "help")
			helpRequested_ = true;
	}

	void displayHelp() {
		Poco::Util::HelpFormatter helpFormatter(options());
		helpFormatter.setCommand(commandName());
		helpFormatter.setUsage("OPTIONS");
		helpFormatter.setHeader("A sample HTTP server supporting the WebSocket protocol.");
		helpFormatter.format(std::cout);
	}

	int main(const std::vector<std::string>& /*args*/) {
		if (helpRequested_) {
			displayHelp();
			return Poco::Util::Application::EXIT_OK;
		}

		// HTTPServer instance
//		port_ = static_cast<std::uint16_t>(config().getInt("WebSocketServer.port", 9981));
		Poco::Net::ServerSocket socket{port_};
		Poco::Net::HTTPServer server{new RequestHandlerFactory, socket, new Poco::Net::HTTPServerParams};

		// start the HTTPServer
		SPDLOG_INFO("Starting server");
		server.start();

		// wait for CTRL-C or kill
		waitForTerminationRequest();

		// Stop the HTTPServer
		SPDLOG_INFO("Stopping server");
		server.stop();

		return Poco::Util::Application::EXIT_OK;
	}

private:
	bool helpRequested_{ false };
	std::uint16_t port_{ 9981 };
};


POCO_SERVER_MAIN(WebSocketServer)
