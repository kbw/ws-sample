//
// WebSocketServer.cpp
//
// This sample demonstrates the WebSocket class.
//
// Copyright (c) 2012, Applied Informatics Software Engineering GmbH.
// and Contributors.
//
// SPDX-License-Identifier:	BSL-1.0
//

#include <strings.h>

#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPServerParams.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPServerParams.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/Net/WebSocket.h>
#include <Poco/Net/NetException.h>
#include <Poco/Util/ServerApplication.h>
#include <Poco/Util/Option.h>
#include <Poco/Util/OptionSet.h>
#include <Poco/Util/HelpFormatter.h>
#include <Poco/Format.h>

#include <spdlog/spdlog.h>

#include <iostream>


using Poco::Net::ServerSocket;
using Poco::Net::WebSocket;
using Poco::Net::WebSocketException;
using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTTPServerParams;
using Poco::Timestamp;
using Poco::ThreadPool;
using Poco::Util::ServerApplication;
using Poco::Util::Application;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Util::HelpFormatter;


#ifdef USE_PAGE_SUPPORT
/// Return a HTML document with some JavaScript creating
/// a WebSocket connection.
class PageRequestHandler : public HTTPRequestHandler {
public:
	void handleRequest(HTTPServerRequest& request, HTTPServerResponse& response) {
		response.setChunkedTransferEncoding(true);
		response.setContentType("text/html");
		std::ostream& ostr = response.send();
		ostr << "<html>";
		ostr << "<head>";
		ostr << "<title>WebSocketServer</title>";
		ostr << "<script type=\"text/javascript\">";
		ostr << "function WebSocketTest()";
		ostr << "{";
		ostr << "  if (\"WebSocket\" in window)";
		ostr << "  {";
		ostr << "    var ws = new WebSocket(\"ws://" << request.serverAddress().toString() << "/ws\");";
		ostr << "    ws.onopen = function()";
		ostr << "      {";
		ostr << "        ws.send(\"Hello, world!\");";
		ostr << "      };";
		ostr << "    ws.onmessage = function(evt)";
		ostr << "      { ";
		ostr << "        var msg = evt.data;";
		ostr << "        alert(\"Message received: \" + msg);";
		ostr << "        ws.close();";
		ostr << "      };";
		ostr << "    ws.onclose = function()";
		ostr << "      { ";
		ostr << "        alert(\"WebSocket closed.\");";
		ostr << "      };";
		ostr << "  }";
		ostr << "  else";
		ostr << "  {";
		ostr << "     alert(\"This browser does not support WebSockets.\");";
		ostr << "  }";
		ostr << "}";
		ostr << "</script>";
		ostr << "</head>";
		ostr << "<body>";
		ostr << "  <h1>WebSocket Server</h1>";
		ostr << "  <p><a href=\"javascript:WebSocketTest()\">Run WebSocket Script</a></p>";
		ostr << "</body>";
		ostr << "</html>";
	}
};
#endif // USE_PAGE_SUPPORT


/// Handle a WebSocket connection.
class WebSocketRequestHandler : public HTTPRequestHandler {
public:
	void handleRequest(HTTPServerRequest& request, HTTPServerResponse& response) {
		Application& app = Application::instance();
		try {
			WebSocket ws(request, response);
//			app.logger().information("WebSocket connection established.");
			spdlog::info("WebSocket connection established.");
			char buffer[1024];
			int flags;
			int nreceived;
			do {
				bzero(buffer, sizeof(buffer));
				nreceived = ws.receiveFrame(buffer, sizeof(buffer), flags);
//				app.logger().information(Poco::format("Frame received (length=%d, flags=0x%x).", nreceived, unsigned(flags)));
				spdlog::info("Frame received (length={}, flags=0x{}): {}", nreceived, flags, buffer);

				while (true) {
					flags = 0;
					ssize_t nsent = ws.sendFrame(buffer, nreceived, flags);
					spdlog::info("Frame sent (length={}, flags=0x{}): {}", nsent, flags, buffer);

					using namespace std::chrono_literals;
					std::this_thread::sleep_for(1ms);
				}
			}
			while (nreceived > 0 && (flags & WebSocket::FRAME_OP_BITMASK) != WebSocket::FRAME_OP_CLOSE);
//			app.logger().information("WebSocket connection closed.");
			spdlog::info("WebSocket connection closed.");
		}
		catch (WebSocketException& exc) {
//			app.logger().log(exc);
			spdlog::warn("{}", exc.code());
			switch (exc.code()) {
			case WebSocket::WS_ERR_HANDSHAKE_UNSUPPORTED_VERSION:
				response.set("Sec-WebSocket-Version", WebSocket::WEBSOCKET_VERSION);
				// fallthrough
			case WebSocket::WS_ERR_NO_HANDSHAKE:
			case WebSocket::WS_ERR_HANDSHAKE_NO_VERSION:
			case WebSocket::WS_ERR_HANDSHAKE_NO_KEY:
				response.setStatusAndReason(HTTPResponse::HTTP_BAD_REQUEST);
				response.setContentLength(0);
				response.send();
				break;
			}
		}
	}
};


class RequestHandlerFactory : public HTTPRequestHandlerFactory {
public:
	HTTPRequestHandler* createRequestHandler(const HTTPServerRequest& request) {
		Application& app = Application::instance();
#ifndef USE_IOSTREAM
		app.logger().information("Request from " 
			+ request.clientAddress().toString()
			+ ": "
			+ request.getMethod()
			+ " "
			+ request.getURI()
			+ " "
			+ request.getVersion());
#endif
		spdlog::info("Request from {}: {} {} {}",
					request.clientAddress().toString(),
					request.getMethod(),
					request.getURI(),
					request.getVersion());
			
		for (HTTPServerRequest::ConstIterator it = request.begin(); it != request.end(); ++it)
			app.logger().information(it->first + ": " + it->second);
		
		if (request.find("Upgrade") != request.end() && Poco::icompare(request["Upgrade"], "websocket") == 0)
			return new WebSocketRequestHandler;

#ifdef USE_PAGE_SUPPORT
		return new PageRequestHandler;
#else
		return nullptr;
#endif // USE_PAGE_SUPPORT
	}
};


/// The main application class.
///
/// This class handles command-line arguments and
/// configuration files.
/// Start the WebSocketServer executable with the help
/// option (/help on Windows, --help on Unix) for
/// the available command line options.
///
/// To use the sample configuration file (WebSocketServer.properties),
/// copy the file to the directory where the WebSocketServer executable
/// resides. If you start the debug version of the WebSocketServer
/// (WebSocketServerd[.exe]), you must also create a copy of the configuration
/// file named WebSocketServerd.properties. In the configuration file, you
/// can specify the port on which the server is listening (default
/// 9980) and the format of the date/time string sent back to the client.
///
/// To test the WebSocketServer you can use any web browser (http://localhost:9980/).
class WebSocketServer : public Poco::Util::ServerApplication {
public:
	WebSocketServer() {
		// https://spdlog.docsforge.com/v1.x/3.custom-formatting/#pattern-flags
		spdlog::set_pattern("%Y-%m-%d %T:%S.%f %^%L%$ %v");
	}
	~WebSocketServer() = default;

protected:
	void initialize(Application& self) {
		loadConfiguration(); // load default configuration files, if present
		ServerApplication::initialize(self);
	}
		
	void uninitialize() {
		ServerApplication::uninitialize();
	}

	void defineOptions(OptionSet& options) {
		ServerApplication::defineOptions(options);
		
		options.addOption(
			Option("help", "h", "display help information on command line arguments")
				.required(false)
				.repeatable(false));
	}

	void handleOption(const std::string& name, const std::string& value) {
		ServerApplication::handleOption(name, value);

		if (name == "help")
			_helpRequested = true;
	}

	void displayHelp() {
		HelpFormatter helpFormatter(options());
		helpFormatter.setCommand(commandName());
		helpFormatter.setUsage("OPTIONS");
		helpFormatter.setHeader("A sample HTTP server supporting the WebSocket protocol.");
//		helpFormatter.format(std::cout);
	}

	int main(const std::vector<std::string>& args) {
		if (_helpRequested) {
			displayHelp();
			return Application::EXIT_OK;
		}

		ServerSocket svs(				// set-up a server socket
			static_cast<uint16_t>(config().getInt("WebSocketServer.port", 9982)));	// get parameters from configuration file
		HTTPServer srv(new RequestHandlerFactory, svs, new HTTPServerParams);		// set-up a HTTPServer instance
		srv.start();					// start the HTTPServer
		waitForTerminationRequest();	// wait for CTRL-C or kill
		srv.stop();						// Stop the HTTPServer

		return Application::EXIT_OK;
	}
	
private:
	bool _helpRequested{};
};


POCO_SERVER_MAIN(WebSocketServer)
