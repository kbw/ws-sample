#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTTPMessage.h>
#include <Poco/Net/WebSocket.h>
#include <Poco/Net/HTTPClientSession.h>

#include <spdlog/spdlog.h>

using Poco::Net::HTTPClientSession;
using Poco::Net::HTTPRequest;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPMessage;
using Poco::Net::WebSocket;


int main(int args, char* argv[]) {
	// https://spdlog.docsforge.com/v1.x/3.custom-formatting/#pattern-flags
	spdlog::set_pattern("%Y-%m-%d %T:%S.%f %^%L%$ %v");

	HTTPClientSession cs("localhost", 9982);
	HTTPRequest request(HTTPRequest::HTTP_GET, "/?encoding=text", HTTPMessage::HTTP_1_1);
	request.set("origin", "localhost");
	HTTPResponse response;

	try {
		WebSocket* m_psock = new WebSocket(cs, request, response);
		constexpr auto buffer = "Hello echo websocket!";
		int nsent = m_psock->sendFrame(buffer, strlen(buffer), WebSocket::FRAME_TEXT);
		spdlog::info("Frame sent (length={}, flags=0x{}): {}", nsent, WebSocket::FRAME_TEXT, buffer);

//		int flags = 0;
//		int rlen = m_psock->receiveFrame(receiveBuff, sizeof(receiveBuff), flags);
//		spdlog::info("Received bytes {}: {}", rlen, receiveBuff);

		for (int i = 0; i < 3; ++i) {
			int flags = 0;
			char receiveBuff[256] = "";
			int nreceived = m_psock->receiveFrame(receiveBuff, sizeof(receiveBuff), flags);
			spdlog::info("Frame received (length={}, flags=0x{}): {}", nreceived, flags, receiveBuff);
			if (nreceived == 0)
				break;
		}

		m_psock->close();
	} catch (std::exception &e) {
		spdlog::error("Fatal: {}", e.what());
	}
}
